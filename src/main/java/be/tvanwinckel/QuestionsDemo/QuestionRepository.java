package be.tvanwinckel.QuestionsDemo;

import be.tvanwinckel.QuestionsDemo.question.Question;

import java.util.List;

public interface QuestionRepository {

    void add(final Question question);

    Question get(final String id);

    List<Question> getAll();
}
