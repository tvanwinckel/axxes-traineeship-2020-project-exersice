package be.tvanwinckel.QuestionsDemo.application;

import be.tvanwinckel.QuestionsDemo.QuestionRepository;
import be.tvanwinckel.QuestionsDemo.exceptions.UnknownQuestionTypeException;
import be.tvanwinckel.QuestionsDemo.question.Question;
import be.tvanwinckel.QuestionsDemo.question.SpectatorQuestion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class QuestionService {

    private final QuestionRepository questionRepository;

    @Autowired
    public QuestionService(final QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    public List<Question> getAll() {
        return questionRepository.getAll();
    }

    public Question add(final String questionContent, final String questionType) {
        if (questionType.equals("SpectatorQuestion")) {
            final String id = UUID.randomUUID().toString();
            final Question question = new SpectatorQuestion(id, questionContent, 0);

            questionRepository.add(question);
            return question;
        }

        throw new UnknownQuestionTypeException();
    }

    public Question add(final Question question) {
        questionRepository.add(question);
        return question;
    }

    public Question get(final String questionId) {
        return questionRepository.get(questionId);
    }
}
