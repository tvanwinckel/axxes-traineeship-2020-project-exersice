package be.tvanwinckel.QuestionsDemo.exceptions;

public class QuestionNotFoundException extends RuntimeException {
    public QuestionNotFoundException(final String message) {
        super(message);
    }
}
