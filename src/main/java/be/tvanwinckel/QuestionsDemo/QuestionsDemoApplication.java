package be.tvanwinckel.QuestionsDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuestionsDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuestionsDemoApplication.class, args);
	}

}
