package be.tvanwinckel.QuestionsDemo.infrastructure;

import be.tvanwinckel.QuestionsDemo.application.QuestionService;
import be.tvanwinckel.QuestionsDemo.exceptions.QuestionNotFoundException;
import be.tvanwinckel.QuestionsDemo.question.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/questions")
public class QuestionController {

    private final QuestionService questionService;

    @Autowired
    public QuestionController(final QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Question> getAllQuestions() {
        return questionService.getAll();
    }

//    @PostMapping
//    public Question addQuestion(@RequestBody final Map<String, String> request) {
//        final String questionContent = request.get("question");
//        final String questionType = request.get("type");
//        return questionService.add(questionContent, questionType);
//    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Question addQuestion(@RequestBody final Question question) {
        return questionService.add(question);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Question getSpecificQuestion(@PathVariable final String id) {
        return questionService.get(id);
    }

    @ExceptionHandler(QuestionNotFoundException.class)
    public String questionNotFoundHandler(final QuestionNotFoundException e) {
        return e.getMessage();
    }
}
