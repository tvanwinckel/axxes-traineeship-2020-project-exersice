package be.tvanwinckel.QuestionsDemo.infrastructure;

import be.tvanwinckel.QuestionsDemo.exceptions.QuestionNotFoundException;
import be.tvanwinckel.QuestionsDemo.QuestionRepository;
import be.tvanwinckel.QuestionsDemo.question.Question;
import be.tvanwinckel.QuestionsDemo.question.SpectatorQuestion;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StaticQuestionRepository implements QuestionRepository {

    private final List<Question> questions = new ArrayList<>();

    public StaticQuestionRepository() {
        questions.add(new SpectatorQuestion("id-12345", "What question is this?", 0));
        questions.add(new SpectatorQuestion("id-67890", "What other question is this?", 0));
    }

    @Override
    public void add(final Question question) {
        questions.add(question);
    }

    @Override
    public Question get(final String id) {
        for (Question question : questions) if (question.getId().equals(id)) return question;
        throw new QuestionNotFoundException("Failed to find question: " + id);
    }

    @Override
    public List<Question> getAll() {
        return questions;
    }
}
