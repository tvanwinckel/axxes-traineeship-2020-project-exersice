package be.tvanwinckel.QuestionsDemo.question;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SpectatorQuestion implements Question {

    private final String id;
    private final String content;
    private final int voteWeight;

    @JsonCreator
    public SpectatorQuestion(final @JsonProperty("id") String id,
                             final @JsonProperty("content") String content,
                             final @JsonProperty("voteWeight") int voteWeight) {
        this.id = id;
        this.content = content;
        this.voteWeight = voteWeight;
    }

    public String getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public int getVoteWeight() {
        return voteWeight;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final SpectatorQuestion that = (SpectatorQuestion) o;

        if (voteWeight != that.voteWeight) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return content != null ? content.equals(that.content) : that.content == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + voteWeight;
        return result;
    }
}
