package be.tvanwinckel.QuestionsDemo.application;

import be.tvanwinckel.QuestionsDemo.QuestionRepository;
import be.tvanwinckel.QuestionsDemo.exceptions.QuestionNotFoundException;
import be.tvanwinckel.QuestionsDemo.exceptions.UnknownQuestionTypeException;
import be.tvanwinckel.QuestionsDemo.question.Question;
import be.tvanwinckel.QuestionsDemo.question.SpectatorQuestion;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class QuestionServiceTest {

    static final String QUESTION_ID_1 = "id-12345";
    static final String QUESTION_ID_2 = "id-67890";

    @Test
    void getAllQuestions() {
        final QuestionService service = initService();
        final List<Question> result = service.getAll();

        assertThat(result.size()).isEqualTo(2);
    }

    @Test
    void getQuestionById() {
        final QuestionService service = initService();
        final Question result = service.get(QUESTION_ID_1);

        final Question expectedResult = new SpectatorQuestion(QUESTION_ID_1, "What question is this?", 0);
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void getQuestionById_noQuestionFound_shouldThrowException() {
        final QuestionService service = initService();

        Assertions.assertThrows(QuestionNotFoundException.class, () -> {
            service.get("UNKNOWN-ID");
        });
    }

    @Test
    void addQuestion() {
        final QuestionService service = initService();
        final Question questionToAdd = new SpectatorQuestion(QUESTION_ID_1, "What question is this?", 0);
        service.add(questionToAdd);

        final List<Question> allQuestions = service.getAll();
        assertThat(questionToAdd).isIn(allQuestions);
    }

    @Test
    void addQuestionThroughContentAndType() {
        final QuestionService service = initService();
        service.add("What question is this?", "SpectatorQuestion");

        final List<Question> allQuestions = service.getAll();
        assertThat(allQuestions.size()).isEqualTo(3);
    }

    @Test
    void addQuestionThroughContentAndType_unknownType() {
        final QuestionService service = initService();

        Assertions.assertThrows(UnknownQuestionTypeException.class, () -> {
            service.add("What question is this?", "UNKNOWN-TYPE");
        });
    }

    private QuestionService initService() {
        return new QuestionService(new QuestionRepositoryStub());
    }

    static class QuestionRepositoryStub implements QuestionRepository {


        private final List<Question> questions = new ArrayList<>();

        public QuestionRepositoryStub() {
            questions.add(new SpectatorQuestion(QUESTION_ID_1, "What question is this?", 0));
            questions.add(new SpectatorQuestion(QUESTION_ID_2, "What other question is this?", 0));
        }

        @Override
        public void add(final Question question) {
            questions.add(question);
        }

        @Override
        public Question get(final String id) {
            for (Question question : questions) if (question.getId().equals(id)) return question;
            throw new QuestionNotFoundException("Failed to find question: " + id);
        }

        @Override
        public List<Question> getAll() {
            return questions;
        }
    }
}
