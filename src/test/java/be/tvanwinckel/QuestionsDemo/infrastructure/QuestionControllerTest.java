package be.tvanwinckel.QuestionsDemo.infrastructure;

import be.tvanwinckel.QuestionsDemo.application.QuestionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(QuestionController.class)
class QuestionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private QuestionService questionService;

    @Test
    void getAllQuestions_returns200() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/questions"))
                .andExpect(status().isOk());
    }

    @Test
    void addQuestion_acceptsJson_return201() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/questions")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void getQuestionbyId_returns200() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/questions/{id}", "1"))
                .andExpect(status().isOk());
    }
}